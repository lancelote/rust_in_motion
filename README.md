# rust_in_motion

My "Rust in Motion" course code

## TOC

- [x] Unit 1: Introduction to the Course and Rust Syntax
- [x] Unit 2: Ownership and Borrowing
  - [Pluralizing Function](src/bin/ownership_exercise.rs)
  - [Pluralizing Function with Borrow](src/bin/borrowing_example.rs)
  - [Entry API](src/bin/entry_api.rs)
  - [New Scope Pattern](src/bin/new_scope.rs)
  - [Temporary Variable Pattern](src/bin/temp_var.rs)
  - [Splitting Tuples Pattern](src/bin/split_tuples.rs)
  - [Managing Sockets](src/bin/managing_sockets.rs)
  - [Drop Trait](src/bin/drop_trait.rs)
- [x] Unit 3: Error Handling
  - [Basic Panic Example](src/bin/panic.rs)
  - [Success and Failure](src/bin/serde_json.rs)
  - [Option Example](src/bin/option.rs)
  - [Match and Result](src/bin/match_result.rs)
  - [Question Mark Operator](src/bin/question_mark.rs)
  - [Question Mark with Option](src/bin/question_mark_option.rs)
  - [Box Error Example](src/bin/box_error.rs)
  - [Custom Error Type](src/bin/custom_error.rs)
  - [`quick-error` Crate](src/bin/quick_error.rs)
  - [`error-chain` Crate](src/bin/error_chain.rs)
  - [`failure` Crate](src/bin/failure_crate.rs)
  - [Unwrapping Value](src/bin/unwrapping_value.rs)
  - [Using Default](src/bin/default_unwrapping.rs)
  - [Transforming Value](src/bin/transform.rs)
- [x] Unit 4: Lifetimes
  - [Basic Lifetimes](src/bin/lifetimes.rs)
  - [Returning Reference from Inner Scope](src/bin/reference_inner.rs)
  - [Returning Reference from Function](src/bin/reference_function.rs)
  - [Referencing a Moved Value](src/bin/reference_moved.rs)
  - [Storing References in `HashMap`](src/bin/references_in_hashmap.rs)
  - [Basic Generics](src/bin/approval.rs)
  - [Lifetime Parameters](src/bin/lifetime_parameters.rs)
  - [More Lifetime Examples](src/bin/more_lifetimes.rs)
