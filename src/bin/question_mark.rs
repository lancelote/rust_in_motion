#[allow(dead_code)]
fn save_status(text: &str) -> Result<i64, &'static str> {
    if text.len() > 200 {
        return Err("status text is too long");
    }

    let record = save_to_database(text)?;

    Ok(record.id)
}

#[allow(dead_code)]
fn save_to_database(_text: &str) -> Result<StatusRecord, &'static str> {
    // fake implementation that always fails
    Err("database unavailable")
}

#[allow(dead_code)]
struct StatusRecord {
    id: i64,
    text: String,
    created_at: std::time::Instant,
}

fn main() {}
