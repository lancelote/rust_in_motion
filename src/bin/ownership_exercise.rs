fn main() {
    let my = String::from("book");
    let yours = pluralize(my.clone());

    println!("I have one {}, you have two {}", my, yours,);
}

fn pluralize(string: String) -> String {
    string + "s"
}
