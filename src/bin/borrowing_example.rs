fn main() {
    let my = String::from("book");
    let yours = pluralize(&my);

    println!("I have one {}, you have two {}", my, yours,);
}

fn pluralize(string: &str) -> String {
    string.to_owned() + "s"
}
